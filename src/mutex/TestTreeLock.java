package mutex;

import java.util.Random;

public class TestTreeLock {
	private static final int requirements = 1000;
	private int threadNumber = Runtime.getRuntime().availableProcessors();
	private long expectedResult = requirements * threadNumber;
	private long computedResult = 0;
	private int meanDelay = 0;
	private double totalTimes;
	private boolean isCorrect = false;
	Thread[] threads = new Thread[threadNumber];
	Random random = new Random();
	PetersonTreeLock treeLock = new PetersonTreeLock(threadNumber);
	
	public TestTreeLock(int meanDelay, int threadNumber) {
		this.meanDelay = meanDelay;
		this.threadNumber = threadNumber;
		this.expectedResult = threadNumber * requirements;
	}
	
	private long nextSleep() {
		return (long) Math.log(1 / (1 - random.nextDouble())) * meanDelay;
	}
	
	class IncrementOne extends Thread {
		public void run() {
			for (int i = 0; i < requirements; i++) {
				treeLock.lock();
				try {
					computedResult += 1;
				} finally {
					treeLock.unlock();
				}
				
				try {
					Thread.sleep(nextSleep());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void test() throws Exception {
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i] = new IncrementOne();
		}
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i].start();
		}
		
		for (int i = 0; i < threadNumber; i++) {
			threads[i].join();
		}
		
		this.isCorrect = (computedResult == expectedResult);
		
		long endTime = System.currentTimeMillis();
		
		this.totalTimes = (double)(endTime - startTime);
	}
	
	public double getRunningTime() {
		return this.totalTimes;
	}
	
	public int getTotalRequests() {
		return requirements * threadNumber;
	}
	
	public boolean isCorrect() {
		return this.isCorrect;
	}
	
	public long getExpectedResult() {
		return this.expectedResult;
	}
	
	public long getComputedResult() {
		return this.computedResult;
	}
	
	public double getThroughput() {
		int totalRequest = requirements * threadNumber;
		
		return totalRequest / totalTimes * 1000;
	}
}
