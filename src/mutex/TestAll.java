package mutex;

public class TestAll {
	private static int delayRange = 100;
	private static int threadNumberRange = Runtime.getRuntime().availableProcessors();
	
	public static void main(String args[]) {
		int rounds;
		if (args.length == 0)
			rounds = 1;
		else
			rounds = Integer.parseInt(args[0]);

		for (int round = 0; round < rounds; round += 1) {
			for (int threads = 1; threads <= threadNumberRange; threads += 1) {
				for (int delay = 10; delay <= delayRange; delay += 10) {
					TestTreeLock testTreeLock = new TestTreeLock(delay, threads);
					try {
						testTreeLock.test();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (testTreeLock.isCorrect()) {
						System.out.printf("%d %d %f\n", 
								threads, delay, testTreeLock.getThroughput());
						System.out.println();
					} else {
						System.out.printf(
								"Wrong!! Expected = %d, Computed = %d\n",
								testTreeLock.getExpectedResult(),
								testTreeLock.getComputedResult());
					}
				}
			}
		}
	}
}
